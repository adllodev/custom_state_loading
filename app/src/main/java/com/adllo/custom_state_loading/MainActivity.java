package com.adllo.custom_state_loading;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.adllo.custom_state_loading.R;
import com.adllo.loading_state.LoadingState;

public class MainActivity extends AppCompatActivity {
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;


        final LoadingState loadingState = (LoadingState) findViewById(R.id.stateLoad);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mContext, "Should be hidden", Toast.LENGTH_LONG).show();
                loadingState.toContent();
            }
        },2000);

        Button button = (Button) findViewById(R.id.btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingState.toLoad();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mContext, "Should be hidden", Toast.LENGTH_LONG).show();
                        loadingState.toContent();
                    }
                },2000);
            }
        });
    }
}
